
// Object Destructuring

// const person = {
//   name: 'Andy',
//   age: 47,
//   location: {
//     city: 'Cardiff',
//     temp: 19
//   }
// };

// const {name: firstName = 'Anon', age} = person;
// const name = person.name;
// const age = person.age;

// console.log(`${firstName} is ${age}.`);

// const {city, temp} = person.location;
// if (city && temp) {
//   console.log(`It's ${temp} in ${city}`);
// }

// const book = {
//   title: 'Ego is the Enemy',
//   author: 'Ryan Holiday',
//   publisher: {
//     name: 'Penguin'
//   }
// };

// const {name: publisherName = 'Self-Published'} = book.publisher;
// if (publisherName) {
//   console.log(publisherName);
// }

// Array Destructuring

const address = ['11 Clos Treoda', 'Whitchurch', 'Cardiff', 'CF14 6DL'];

const [, city, region] = address;

console.log(`You are in ${city} ${region}.`);

const item  = ['Coffee (iced)', '£2', '£2.85', '£2.75'];

const [drink, , medium] = item;

console.log(`A medium ${drink} cost ${medium}.`);